<?php

include_once __DIR__."/../model/Article.php";
date_default_timezone_set("Europe/Kiev");

if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {

    (new Article())->insert($_POST['name'], $_POST['description'], $_POST['created_at']);

    header('Location: ../index.php');
}
 $row['name'] = '';
 $row['description'] = '';
 $row['created_at'] = date('Y-m-d H:i:s', time());
 $id = '';
require_once __DIR__."/../view/editTemplate.php";
